const BASE_URL = "https://63b2c99f5901da0ab36dbaed.mockapi.io";
// gọi api lấy danh sách món ăn từ server
function fetchFoodList() {
  batLoading();
  axios({
    url: `${BASE_URL}/food`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      renderFoodList(res.data);
    })
    .catch(function (err) {
      tatLoading();
    });
}
// chạy lần đầu khi load trang
fetchFoodList();

// xoá món ăn
function xoaMonAn(id) {
  batLoading();
  axios({
    url: `${BASE_URL}/food/${id}`,
    method: "DELETE",
  })
    .then(function (res) {
      tatLoading();
      // gọi lại axios lấy danh sách món ăn từ server sau khi xoá thành công
      fetchFoodList();
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

function themMonAn() {
  batLoading();
  axios({
    url: `${BASE_URL}/food`,
    method: "POST",
    data: layThongTinTuForm(),
  })
    .then(function (res) {
      tatLoading();
      // gọi lại api lấy danh sách món ăn sau khi thêm thành công
      fetchFoodList();
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

function suaMonAn(id) {
  console.log(`  🚀: suaMonAn -> id`, id);
  batLoading();
  axios({
    url: `${BASE_URL}/food/${id}`,
    method: "GET",
  })
    .then(function (res) {
      tatLoading();
      console.log(res);
      showThongTinLenForm(res.data);
    })
    .catch(function (err) {
      tatLoading();
      console.log(err);
    });
}

function capNhatMonAn() {
  let data = layThongTinTuForm();
  batLoading();
  axios({
    url: `${BASE_URL}/food/${data.maMon}`,
    method: "PUT",
    data: data,
  })
    .then(function (res) {
      console.log(res);
      tatLoading();
      // gọi lại danh sách món ăn sau khi cậpn nhật thành công
      fetchFoodList();
    })
    .catch(function (err) {
      console.log(err);
      tatLoading();
    });
}
