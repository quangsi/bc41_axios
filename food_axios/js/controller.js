function batLoading() {
  document.getElementById("loading").style.display = "flex";
}

function tatLoading() {
  document.getElementById("loading").style.display = "none";
}

// render danh sách món ăn
function renderFoodList(foodArr) {
  console.log(`  🚀: renderFoodList -> foodArr`, foodArr);
  var contentHTML = "";

  foodArr.forEach(function (food) {
    console.log(`  🚀: renderFoodList -> food`, food);
    var contentTr = `<tr>
                          <td>${food.maMon}</td>
                          <td>${food.tenMon}</td>
                          <td>${food.giaMon}</td>
                          <td>${food.loaiMon}</td>
                          <td>${food.hinhAnh}</td>
                          <td>
                            <button
                            onclick="xoaMonAn(${food.maMon})"
                            class="btn btn-danger">Xoá</button>
                            <button
                            onclick="suaMonAn(${food.maMon})"
                            class="btn btn-warning">Sửa</button>
                          </td>

                     </tr>`;
    contentHTML = contentHTML + contentTr;
  });
  // dom tới tbodyFood
  document.getElementById("tbodyFood").innerHTML = contentHTML;
}

function layThongTinTuForm() {
  var maMon = document.getElementById("maMon").value;
  var tenMon = document.getElementById("tenMon").value;
  var giaMon = document.getElementById("giaMon").value;
  var hinhAnh = document.getElementById("hinhAnh").value;
  var loaiMon = document.getElementById("loaiMon").value;
  return {
    maMon: maMon,
    tenMon: tenMon,
    giaMon: giaMon,
    hinhAnh: hinhAnh,
    loaiMon: loaiMon,
  };
}

function showThongTinLenForm(food) {
  document.getElementById("maMon").value = food.maMon;
  document.getElementById("tenMon").value = food.tenMon;
  document.getElementById("giaMon").value = food.giaMon;
  document.getElementById("hinhAnh").value = food.hinhAnh;
  document.getElementById("loaiMon").value = food.loaiMon;
}
