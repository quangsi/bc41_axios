//  đồng độ vs bất đồng bộ
setTimeout(function () {
  console.log("hello");
}, 5000);
setTimeout(function () {
  console.log(0);
}, 0);
console.log(2);
console.log(1);

// bất đồng bộ

// 0 hay 1 in ra trước
// pending ,success ( resolve ), fail ( reject )

axios({
  url: "https://63b2c99f5901da0ab36dbaed.mockapi.io/food",
  method: "GET",
})
  .then(function (res) {
    // response
    console.log(`  🚀: res`, res.data);
  })
  .catch(function (err) {
    console.log(`  🚀: err`, err);
  });
